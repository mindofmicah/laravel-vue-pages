<?php
namespace MindOfMicah\LaravelVuePages;

use MindOfMicah\LaravelVuePages\Commands\InstallCommand; 
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class, 
            ]);
        }

        $this->loadViewsFrom(__DIR__. '/../resources/views', 'vue-pages');
        $this->publishes([
            __DIR__ . '/../resources/views'=>resource_path('views/vendor/vue-pages'),
        ], 'vue-pages:views');

        $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('vue-pages.php')
        ], 'vue-pages:config');


        $config = $this->app['config']['vue-pages'];
        $this->app['view']->macro($config['macro'], function (string $name, array $data = []) use ($config) {
            return $this->make($config['view'], compact('name', 'data'));
        });
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'vue-pages');
    }
}
