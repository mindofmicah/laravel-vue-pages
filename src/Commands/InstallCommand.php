<?php
namespace MindOfMicah\LaravelVuePages\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'vue-pages:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add a JS snippet to the main app file to register the pages';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Filesystem $filesystem)
    {
        $contents = str_replace(
            '{{DIRECTORY}}', 
            $this->argument('directory'),
            $filesystem->get(__DIR__ . '/../../resources/stubs/js.stub')
        );

        $filesystem->append($this->argument('app_file'), $contents);

        $this->info('JS snippet appended to ' . $this->argument('app_file'));
    }

    public function getArguments()
    {
        return [
            ['app_file', InputArgument::OPTIONAL, 'javascript used as the root application', resource_path('js/app.js')],
            ['directory', InputArgument::OPTIONAL, 'directory used for loading the pages', 'vue-pages'],
        ];
    }
}
