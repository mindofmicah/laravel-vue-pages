@extends('layouts.app')

@section('content')
<div data-vue-page="{{$name}}" data-props="{{json_encode($data)}}">
</div>
@endsection
